package MusicServer.classes;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import Interfaces.InterfaceProject;
import classes.Song;

public class MusicServer extends UnicastRemoteObject implements InterfaceProject {

    private Song[] listSongs = {
            new Song("Clocks", "ColdPlay"),
            new Song("Fix You", "ColdPlay"),
            new Song("My Universe", "ColdPlay"),
    };

    public MusicServer() throws RemoteException {
    }

    public ArrayList<Song> getSongs(String search) throws RemoteException {
        ArrayList<Song> returnList = new ArrayList<>();

        for (Song song : listSongs) {
            if (song.getName().toLowerCase().contains(search.toLowerCase())) {
                returnList.add(song);
            }
        }

        /* Song[] returnArray = returnList.toArray(new Song[returnList.size()]); */
        return returnList;
    }

}
