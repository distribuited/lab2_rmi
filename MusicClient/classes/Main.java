package MusicClient.classes;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Scanner;

import classes.Song;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberOption;
        String songName;
        ArrayList<Song> result = new ArrayList<Song>();
        String menu = "\n+++++[Opciones]+++++\n\n[-1] = Salir\n[0] = Buscar canción\nElige: ";
        MusicClient musicClient = new MusicClient();
        do {
            System.out.println(menu);
            try {
                numberOption = Integer.parseInt(sc.nextLine());
            } catch (NumberFormatException e) {
                numberOption = -1;
            }

            if (numberOption != -1) {

                System.out.println("Ingresa el nombre de la canción que quieres buscar: ");
                songName = sc.nextLine();
                try {
                    // instance of musicClient
                    result = musicClient.search(songName);
                } catch (RemoteException | MalformedURLException e) {
                    e.printStackTrace();
                }
                for (Song song : result) {
                    System.out.println("Resultados = " + (song));
                }
                System.out.println("Presiona ENTER para hacer otra búsqueda");
                sc.nextLine();
            }
        } while (numberOption != -1);
        sc.close();
    }
}
