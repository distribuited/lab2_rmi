package MusicClient.classes;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import Interfaces.InterfaceProject;
import classes.Song;

public class MusicClient {

    InterfaceProject interfaceProject;

    public MusicClient() {
        try {
            interfaceProject = (InterfaceProject) Naming.lookup("rmi://127.0.0.1:1802/musicserver");
        } catch (MalformedURLException | RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Song> search(String searchSong) throws RemoteException, MalformedURLException {
        ArrayList<Song> songsFound = interfaceProject.getSongs(searchSong);
        if (songsFound.size() > 0) {
            return songsFound;
        } else {
            System.out.println("Nothing found");
            return songsFound;
        }
    }
}
