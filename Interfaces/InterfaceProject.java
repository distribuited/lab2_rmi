package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import classes.Song;

public interface InterfaceProject extends Remote {
    public ArrayList<Song> getSongs(String search) throws RemoteException;
}