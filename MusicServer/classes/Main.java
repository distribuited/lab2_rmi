package MusicServer.classes;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class Main {
    public static void main(String[] args) {
        try {
            MusicServer myMusicServer = new MusicServer();
            LocateRegistry.createRegistry(1802);
            Naming.rebind("rmi://127.0.0.1:1802/musicserver", myMusicServer);
            System.out.println("[Server]: Server initialized.");
        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
